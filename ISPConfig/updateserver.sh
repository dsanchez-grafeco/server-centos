#!/bin/bash
##################################

# Created by:
# https://dario.sanchezdorado.com


##################################

# Commands to help you:
# chown -R user:group directory
# find / -type d -name "directory" 2>/dev/null
# find / -type f -name "file.php" 2>/dev/null
# grep -r -i "100.100.100.100" /  2>/dev/null
# cd /var/www/clients/client1/
# mysqldump -u user --password=the_pass database > database.sql
# mysql -u [user] -p[the_pass] [database_name] < [filename].sql
# zip -FSor archivo.zip /ruta_a_comprimir

####
# 10 archivos de más tamaño
# find / -type d -printf '%s %p\n'| sort -nr | head -10
####


##################################

# CRON ISPConfig
# /usr/local/ispconfig/server/server.sh 2>&1 | while read line; do echo `/bin/date` "$line" >> /var/log/ispconfig/cron.log; done

##################################

# Renew SSL certificate


# Disabled Let's Encrypt for this site in ispconfig Admin Panel

# Removed the cert and the renewal file in /etc/letsencrypt/renewal and /etc/letsencrypt/certs for this site:
# rm -rf /etc/letsencrypt/renewal/ispconfig.domain.net.conf

# Enabled let's encrypt for the site again in ispconfig configuration.


##################################

## Change admin password in ISPConfig

## Login to the mysql database.
# mysql -u root -pPassword

## Then enter the password of the mysql root user. To switch to the ISPConfig database, run this command:
# use dbispconfig;

## And execute the SQL command:
# UPDATE sys_user SET passwort = md5('admin') WHERE username = 'admin';
# UPDATE `server` SET `server_name` = 'grafeco.guest' WHERE `server`.`server_id` = 1;
# UPDATE `server_ip` SET `ip_address` = '100.100.100.100' WHERE `server_ip`.`server_ip_id` = '1';

## Finally close the mysql shell:
# quit;

##################################

# See cron ISPConfig
# /usr/local/ispconfig/server/server.sh

##################################


export LC_ALL="en_US.UTF-8"
sudo locale-gen "en_US.UTF-8"
sudo locale-gen --purge en_US.UTF-8
echo -e 'LANG="en_US.UTF-8"\nLANGUAGE="en_US:en"\n' > /etc/default/locale


apt-get -y update
apt-get -y upgrade

# php
apt-get -y install -y apt-transport-https lsb-release ca-certificates
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
apt-get -y update
apt-get -y upgrade

# php 5.6
apt-get -y install php5.6 php5.6-cli php5.6-cgi php5.6-fpm php5.6-gd php5.6-mysql php5.6-imap php5.6-curl php5.6-intl php5.6-pspell php5.6-recode php5.6-sqlite3 php5.6-tidy php5.6-xmlrpc php5.6-xsl php5.6-zip php5.6-mbstring php5.6-soap php5.6-opcache libicu65 php5.6-common php5.6-json php5.6-readline php5.6-xml
a2enmod proxy_fcgi setenvif
a2enconf php5.6-fpm

# php 7.0
apt-get -y install php7.0 php7.0-cli php7.0-cgi php7.0-fpm php7.0-gd php7.0-mysql php7.0-imap php7.0-curl php7.0-intl php7.0-pspell php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-zip php7.0-mbstring php7.0-soap php7.0-opcache php7.0-common php7.0-json php7.0-readline php7.0-xml
a2enmod proxy_fcgi setenvif
a2enconf php7.0-fpm

# php 7.1
apt-get -y install php7.1 php7.1-cli php7.1-cgi php7.1-fpm php7.1-gd php7.1-mysql php7.1-imap php7.1-curl php7.1-intl php7.1-pspell php7.1-recode php7.1-sqlite3 php7.1-tidy php7.1-xmlrpc php7.1-xsl php7.1-zip php7.1-mbstring php7.1-soap php7.1-opcache php7.1-common php7.1-json php7.1-readline php7.1-xml
a2enmod proxy_fcgi setenvif
a2enconf php7.1-fpm

# php 7.2
apt-get -y install php7.2 php7.2-cli php7.2-cgi php7.2-fpm php7.2-gd php7.2-mysql php7.2-imap php7.2-curl php7.2-intl php7.2-pspell php7.2-recode php7.2-sqlite3 php7.2-tidy php7.2-xmlrpc php7.2-xsl php7.2-zip php7.2-mbstring php7.2-soap php7.2-opcache php7.2-common php7.2-json php7.2-readline php7.2-xml
a2enmod proxy_fcgi setenvif
a2enconf php7.2-fpm

# php 7.3
apt-get -y install php7.3 php7.3-cli php7.3-cgi php7.3-fpm php7.3-gd php7.3-mysql php7.3-imap php7.3-curl php7.3-intl php7.3-pspell php7.3-recode php7.3-sqlite3 php7.3-tidy php7.3-xmlrpc php7.3-xsl php7.3-zip php7.3-mbstring php7.3-soap php7.3-opcache php7.3-common php7.3-json php7.3-readline php7.3-xml
a2enmod proxy_fcgi setenvif
a2enconf php7.3-fpm

# php 7.4
apt-get -y install php7.4 php7.4-cli php7.4-cgi php7.4-fpm php7.4-gd php7.4-mysql php7.4-imap php7.4-curl php7.4-intl php7.4-pspell php7.4-sqlite3 php7.4-tidy php7.4-xmlrpc php7.4-xsl php7.4-zip php7.4-mbstring php7.4-soap php7.4-opcache libonig5 php7.4-common php7.4-json php7.4-readline php7.4-xml
a2enmod proxy_fcgi setenvif
a2enconf php7.4-fpm

systemctl reload apache2

service php5.6-fpm restart
service php7.0-fpm restart
service php7.1-fpm restart
service php7.2-fpm restart
service php7.3-fpm restart
service php7.4-fpm restart

# Memcache
apt-get -y install php-memcache php-memcached
#pecl channel-update pecl.php.net

#php default version
sudo update-alternatives --set php /usr/bin/php7.3

# check php defaltul version
php -v