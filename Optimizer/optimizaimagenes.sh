
# Install jpegoptim
# sudo yum -y install https://extras.getpagespeed.com/release-latest.rpm
# sudo yum -y install jpegoptim

#Optimizo todas las imágenes jpg de esta carpeta.
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/img/ -name '*.jpg' -type f -print0 | xargs -0 jpegoptim -o --strip-all
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/themes/ -name '*.jpg' -type f -print0 | xargs -0 jpegoptim -o --strip-all
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/modules/ph_simpleblog/ -name '*.jpg' -type f -print0 | xargs -0 jpegoptim -o --strip-all
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/modules/simpleslideshow/ -name '*.jpg' -type f -print0 | xargs -0 jpegoptim -o --strip-all


# Install optipng
# yum install optipng

# Optimizo todas las imágenes png de esta carpeta.
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/img/ -name '*.png' -type f -print0 | xargs -0 optipng
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/themes/ -name '*.png' -type f -print0 | xargs -0 optipng
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/modules/ph_simpleblog/ -name '*.png' -type f -print0 | xargs -0 optipng
# find /var/www/vhosts/domain.com/httpdocs/Web/Shop/modules/simpleslideshow/ -name '*.png' -type f -print0 | xargs -0 optipng


#!/bin/bash


# Optimizar todas las imágenes con libwebp de Google
# yum install libwebp
# sh optimizarimagenes.sh /var/www/vhosts/domain.com/httpdocs/Web/Shop/img/
# sh /home/user/domains/domain.com/scripts/optimizarimagenes.sh /home/user/domains/domain.com/public_html/img/
# 49      5       *       *       0       sh /home/user/domains/domain.com/scripts/optimizarimagenes.sh /home/user/domains/domain.com/public_html/img/



# En Ubuntu 16.04, puede instalar cwebp, una herramienta que comprime imágenes al formato .webp al escribir lo siguiente:

# sudo apt-get update
# sudo apt-get install webp


# En CentOS 7, escriba lo siguiente:

# sudo yum install libwebp-tools

# info: https://www.digitalocean.com/community/tutorials/como-crear-y-presentar-imagenes-webp-para-acelerar-su-sitio-web-es

echo Optimizando...

# converting JPEG images
find $1 -type f -and \( -iname "*.jpg" -o -iname "*.jpeg" \) \
-exec bash -c '
webp_path=$(sed 's/\.[^.]*$/.webp/' <<< "$0");
if [ ! -f "$webp_path" ]; then
  cwebp -quiet -q 90 "$0" -o "$webp_path";
fi;' {} \;


find $1 -type f -and -iname "*.png" \
-exec bash -c '
webp_path=$(sed 's/\.[^.]*$/.webp/' <<< "$0");
if [ ! -f "$webp_path" ]; then
  cwebp -quiet -lossless "$0" -o "$webp_path";
fi;' {} \;


echo Proceso completado.
